import { Component, OnInit, Input } from '@angular/core';
import { Event } from '@angular/router';
import { ApiServicesService } from 'src/app/Services/api-services.service';
import Swal from 'sweetalert2';
import { UsuarioEntities } from '../../Entities/UsuarioEntites';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  usuario: Array<object>;

  constructor(private service: ApiServicesService) { }

  ngOnInit() {
     this.service.GetUsuarios().subscribe(data=>{
       this.usuario = data['parametros'];
      
    });
  }

  eliminar(usuario: number){
    console.log(usuario);
    Swal.fire({
      title: 'Esta seguro que desea eliminar?',
      text: "Esta operación es irreversible!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Eliminado!',
          'Registro eliminado exitosamente',
          'success'
        )
        this.service.EliminarEmpleado(usuario).subscribe(data=>{
      
    
          this.ngOnInit();
        });
      }
    })
   
  }

}
