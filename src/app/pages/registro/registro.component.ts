import { Component, OnInit } from '@angular/core';
import { UsuarioEntities } from 'src/app/Entities/UsuarioEntites';
import { NgForm } from '@angular/forms';
import { ApiServicesService } from '../../Services/api-services.service';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {


  usuario : UsuarioEntities;
  respuesta:any;

  constructor(private service: ApiServicesService, private router: Router) { }

  ngOnInit() {

    this.usuario = new UsuarioEntities();
    

   }

   

   onSubmit(Usuario: NgForm){
     
     if (Usuario.invalid) {
       return;
     }
     Swal.fire({
      allowOutsideClick: false,
      text : 'Espere por favor',
      icon : "info"
     });
     Swal.showLoading()
      this.service.GuardarUsuario(this.usuario).subscribe(data=>{
        
       if (data['codigo'] == "200") {
        Swal.close();
        Swal.fire(
          'Bienvenido!',
          data['descripcion'],
          'success'
        )
        
        
        this.router.navigateByUrl('/login')
       }
       else{
        Swal.close();
        Swal.fire(
          'Error!',
          data['descripcion'],
          'error'
        )
       }
       
      })
      

      
     
     
   }

}
