import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioEntities } from '../../Entities/UsuarioEntites';
import Swal from 'sweetalert2';
import { ApiServicesService } from 'src/app/Services/api-services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  Usuario: UsuarioEntities;
  @Output() UsuarioLogueado: EventEmitter<object>;

  constructor(private service: ApiServicesService, private router: Router) {
    this.UsuarioLogueado = new EventEmitter();
   }

  ngOnInit() {
    this.Usuario = new UsuarioEntities();
  }

  onSubmit(usuario : NgForm){
    console.log('hola');
    
    if (usuario.invalid) {
      return;
    }
    https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.all.min.js
    Swal.fire({
      allowOutsideClick: false,
      text : 'Espere por favor',
      icon : "info"
     });
     Swal.showLoading()
     this.service.LoginUsuario(this.Usuario).subscribe(data=>{
       
      if (data['token'] != '') {
        Swal.close();
        Swal.fire(
          'Bienvenido!',
          'Usted ingreso como ' + this.Usuario.Usuario,
          'success'
        )
        this.router.navigateByUrl('/home');
        this.GuardarToken(data['token']);
      }
      else
      {
        Swal.close();
        Swal.fire(
          'Error!',
          'Usuario Y/O contraseña invalida',
          'error'
        )
      }
     });
  }

 private  GuardarToken(token: string){
      localStorage.setItem('Token', token)
  }

}
