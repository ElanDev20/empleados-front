import { Injectable } from '@angular/core';
import { UsuarioEntities } from '../Entities/UsuarioEntites';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  // private URL  = 'https://biblioteca.azurewebsites.net/api/Autor';
  private URL  = 'https://localhost:44327/api/Empleado';
  private URLLoñgin  = 'https://localhost:44327/api';
  // m.bonilla@smart.edu.co

  private headers = new HttpHeaders().set('Content-Type','application/json');

  constructor(private http: HttpClient) { }

  GuardarUsuario(usuario: UsuarioEntities){
    console.log(usuario);
    
    return this.http.post(`${ this.URL }/Registro`,JSON.stringify(usuario), { headers: this.headers });
  }

  LoginUsuario(usuario: UsuarioEntities){
    return this.http.post(`${ this.URLLoñgin }/Login`,JSON.stringify(usuario), { headers: this.headers });
  }

  GetUsuarios(){
    return this.http.get(`${ this.URL }/GetUsuarios`);
  }

  EliminarEmpleado(usuario : number){
    return this.http.delete(`${ this.URL }/`+ usuario);
  }

}
